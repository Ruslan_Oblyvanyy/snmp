from pysnmp.hlapi import *
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import os

'''scope = ['https://spreadsheets.google.com/feeds']
credentials = ServiceAccountCredentials.from_json_keyfile_name('SNMP-928975a18b3d.json', scope)
gc = gspread.authorize(credentials)
wksh = gc.open("ЛК")
wks = wksh.get_worksheet(1)
dict = wks.get_all_records(empty2zero=False, head=1, default_blank='')

i = 0
while i<len(dict):
    ip = str(dict[i].get('ip адрес'))
    print(ip)
    i = i + 1'''

try:
    errorIndication, errorStatus, errorIndex, varBinds = next(getCmd(SnmpEngine(),
       CommunityData('public', mpModel=0),
       UdpTransportTarget(('194.44.15.142', 161)),
       ContextData(),
       ObjectType(ObjectIdentity('MIKROTIK-MIB', '1.3.6.1.2.1.2.2.1.6.1', 0)))
    )
except:
    pass

if errorIndication:
    print(errorIndication)
elif errorStatus:
    print('%s at %s' % (errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex) - 1][0] or '?'))
else:
    for varBind in varBinds:
        print(' = '.join([x.prettyPrint() for x in varBind]))
